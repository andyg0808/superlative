Superlative is a quick script to help in running blind studies of audio quality.

# Usage
```
superlative audio1.mp3 audio2.mp3...
```

Superlative will shuffle all the files presented on the command line, then play them once each in a random order, giving the user a chance to hear them all. Then it will play them completely randomly, selecting with replacement until it has played ten times.